import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class MenuItemsService {

  constructor() { }

  menuItems = [
    {
      id: 1, name: 'Burger', description: 'A classic hamburger',
      price: 5.99
    },
    {
      id: 2, name: 'Fries', description: 'Crispy french fries',
      price: 2.99
    },
    {
      id: 3, name: 'Soda', description: 'A cold soda', price: 1.99
    }
  ];

  getAllMenuItems(){
    return this.menuItems;
  }

  getMenuItem(id: number) {
    return this.menuItems.find((menuItem) => menuItem.id == id);
  }
}
