import { Component } from '@angular/core';
import { MenuItemsService } from '../providers/menu-items.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {

  menuItems: Array<any> = [];

  constructor(private menuItemsService: MenuItemsService) {
  }
  ngOnInit() {
    this.menuItems = this.menuItemsService.getAllMenuItems();
  }
}
