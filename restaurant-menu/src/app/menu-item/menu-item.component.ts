import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuItemsService } from '../providers/menu-items.service';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.css']
})
export class MenuItemComponent {
  menuItem!: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private menuItemsService: MenuItemsService
  ) { }
  
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      let id = params['id'];
      this.menuItem = this.menuItemsService.getMenuItem(id);
    });
  }
}
