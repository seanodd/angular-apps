import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { MenuComponent } from './menu/menu.component';

const routes: Routes = [  { 
  path: "", component: MenuComponent },
{ path: "menu", component: MenuComponent },
{ path: "menu-item", component: MenuItemComponent },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
