import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from './../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { };

  // set up the endpoint and any HTTP headers
  private userEndpoint: string =
    'https://jsonplaceholder.typicode.com/users';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getUsers(): Observable<User[]> {
    return this.http.get(this.userEndpoint,
      this.httpOptions)
      .pipe(map(res => <User[]>res));
  }
  getUser(id: number): Observable<User> {
    return this.http.get(this.userEndpoint + "/" + id,
      this.httpOptions)
      .pipe(map(res => <User>res));
  }
}




