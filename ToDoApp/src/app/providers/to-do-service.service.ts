import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToDo } from './../models/to-do.model';

@Injectable({
  providedIn: 'root'
})
export class ToDoService {

  constructor(private http: HttpClient) { };

  // set up the endpoint and any HTTP headers
  private todoEndpoint: string =
    'https://jsonplaceholder.typicode.com/todos';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getToDos(): Observable<ToDo[]> {
    return this.http.get(this.todoEndpoint,
      this.httpOptions)
      .pipe(map(res => <ToDo[]>res));
  }
  getToDo(id: number): Observable<ToDo> {
    return this.http.get(this.todoEndpoint + "/" + id,
      this.httpOptions)
      .pipe(map(res => <ToDo>res));
  }
}




