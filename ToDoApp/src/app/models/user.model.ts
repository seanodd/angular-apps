
import { Address } from './address.model';
import { Company } from './company.model';

export class User {

    public id!: number;
    public name: string = '';
    public email: string = '';
    public company!: Company;
    public address!: Address;
    // constructor(userId: number, id: number, title: string, completed: string) {
    //     this.userId = userId;
    //     this.id = id;
    //     this.title = title;
    //     this.completed = completed;
    // }
}


// {
//     "id": 1,
//     "name": "Leanne Graham",
//     "username": "Bret",
//     "email": "Sincere@april.biz",
//     "address": {
//       "street": "Kulas Light",
//       "suite": "Apt. 556",
//       "city": "Gwenborough",
//       "zipcode": "92998-3874",
//       "geo": {
//         "lat": "-37.3159",
//         "lng": "81.1496"
//       }
//     },
//     "phone": "1-770-736-8031 x56442",
//     "website": "hildegard.org",
//     "company": {
//       "name": "Romaguera-Crona",
//       "catchPhrase": "Multi-layered client-server neural-net",
//       "bs": "harness real-time e-markets"
//     }
//   },