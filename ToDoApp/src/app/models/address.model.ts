export class Address {
    public street: string = '';
    public city: string = '';
    public zipcode: string = '';
}
