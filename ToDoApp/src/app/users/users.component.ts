import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { UsersService } from '../providers/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

  export class UsersComponent implements OnInit {
    users: Array<User> = [];
    constructor(private usersService: UsersService) { }
    ngOnInit() {
      // call getToDos() method in ToDoService
      this.usersService.getUsers().subscribe(data => {
        this.users = data;
      });
    }
  }

