import { Component, OnInit } from '@angular/core';
import { ToDoService } from '../providers/to-do-service.service';
import { ToDo } from '../models/to-do.model';
@Component({
  selector: 'app-to-dos',
  templateUrl: './to-dos.component.html',
  styleUrls: ['./to-dos.component.css']
})

export class ToDosComponent implements OnInit {
  todos: Array<ToDo> = [];
  constructor(private todoService: ToDoService) { }
  ngOnInit() {
    // call getToDos() method in ToDoService
    this.todoService.getToDos().subscribe(data => {
      this.todos = data;
    });
  }
}