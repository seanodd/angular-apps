import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Service } from './../models/service.model';

@Injectable({
  providedIn: 'root'
})


export class ServicesService {

  constructor(private http: HttpClient) { };

  // set up the endpoint and any HTTP headers
  private serviceEndpoint: string =
    'http://localhost:8081/api/services';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };


  getServices(): Observable<Service[]> {
    return this.http.get(this.serviceEndpoint,
      this.httpOptions)
      .pipe(map(res => <Service[]>res));
  }
  // getServicesByCategory(catId: string): Observable<Service[]> {
  //   return this.http.get(this.serviceEndpoint,
  //     this.httpOptions)
  //     ..pipe(map(res => <Service[]>res));
  //     // return this.http.get(this.serviceEndpoint,
  //     //   this.httpOptions)
  //     //   .pipe(map(res => res.).filter(serv => serv.CatId = "ACU1")));
  // }
  getService(id: number): Observable<Service> {
    return this.http.get(this.serviceEndpoint + "/" + id,
      this.httpOptions)
      .pipe(map(res => <Service>res));
      // filter(serv => serv.CatId == "ACU1"));
  }
}