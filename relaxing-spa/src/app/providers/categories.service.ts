import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Category } from './../models/category.model';


@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) { };

  // set up the endpoint and any HTTP headers
  private categoryEndpoint: string =
    'http://localhost:8081/api/categories';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };


  getCategories(): Observable<Category[]> {
    return this.http.get(this.categoryEndpoint,
      this.httpOptions)
      .pipe(map(res => <Category[]>res));
  }
  getCategory(id: number): Observable<Category> {
    return this.http.get(this.categoryEndpoint + "/" + id,
      this.httpOptions)
      .pipe(map(res => <Category>res));
  }
}
