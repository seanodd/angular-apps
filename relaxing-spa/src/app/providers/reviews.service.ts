import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Review } from './../models/review.model';

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {

  constructor(private http: HttpClient) { };

  // set up the endpoint and any HTTP headers
  private reviewEndpoint: string =
    'http://localhost:8081/api/reviews';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getReviews(): Observable<Review[]> {
    return this.http.get(this.reviewEndpoint,
      this.httpOptions)
      .pipe(map(res => <Review[]>res));
  }
  getReview(id: number): Observable<Review> {
    return this.http.get(this.reviewEndpoint + "/" + id,
      this.httpOptions)
      .pipe(map(res => <Review>res));
  }
  // register a review and pass the server the values as:
  // ReviewName=some-review&Coach=some-coach&CoachEmail=some-email
  postNewReview(reviewer: string, comment: string): Observable<Review> {
    console.log("postNewReview - comment: " + comment);
    return this.http.post<Review>(this.reviewEndpoint,
      {
        reviewer: reviewer,
        comment: comment
      },
      this.httpOptions);
  }

}
