import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Service } from '../models/service.model';
import { ServicesService } from '../providers/services.service';

@Component({
    selector: 'app-service-details',
    templateUrl: './service-details.component.html',
    styleUrls: ['./service-details.component.css']
})
export class ServiceDetailsComponent {
    service!: Service;

    constructor(
        private activatedRoute: ActivatedRoute,
        private servicesService: ServicesService
    ) { }

    ngOnInit() {
        this.activatedRoute.queryParams.subscribe((params) => {
            let id = params['id'];
            this.servicesService.getService(id).subscribe((data: Service) => {
                //console.log(data);
                this.service = data;
            })
        }
        )
    }
}
