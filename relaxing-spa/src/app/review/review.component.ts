import { Component } from '@angular/core';
import { Review } from '../models/review.model';
import { ReviewsService } from '../providers/reviews.service';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent {
  reviewer: string = "";
  comment: string = "";
  review!: Review;
  constructor(
    private reviewsService: ReviewsService
  ) { }

  onSubmitReviewClicked(): void {

    this.reviewsService.postNewReview(this.reviewer, this.comment).subscribe((data: Review) => {
      //console.log(data);
      this.review = data;
    });

  }
}
