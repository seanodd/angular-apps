export class Service {
    public ServiceID: string = '';
    public ServiceName: string = '';
    public Description: string = '';
    public Price: string = '';
    public Minutes: string = '';
    public CategoryName: string = '';
}

// {"ServiceID":"SKIN1",
// "ServiceName":"Double Lift Pure Bliss",
// "Description":"We will lift, plump, firm and hydrate your face with the finest ingredients.",
// "Price":"250.00",
// "Minutes":"110",
// "CategoryName":"Skin Care"}