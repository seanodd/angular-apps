export class Review {
    public date: string = '';
    public reviewer: string = '';
    public comment: string = '';
}

