import { Component } from '@angular/core';
import { Review } from '../models/review.model';
import { ReviewsService } from '../providers/reviews.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent {
  
  reviews: Array<Review> = [];

  constructor(
    private reviewsService: ReviewsService
  ) { }
  ngOnInit() {

      this.reviewsService.getReviews().subscribe((data)=>{
        //console.log(data);
        // TODO
        this.reviews = data;  
     });

  }
}