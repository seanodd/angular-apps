import { Component, OnInit } from '@angular/core';
import { Category } from '../models/category.model';
import { Service } from '../models/service.model';
import { CategoriesService } from '../providers/categories.service';
import { ServicesService } from '../providers/services.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {
  categories: Array<Category> = [];
  matchingServices: Array<Service> = [];
  constructor(private categoriesService: CategoriesService,
    private servicesService: ServicesService) { }
  ngOnInit() {
    // call getToDos() method in ToDoService
    this.categoriesService.getCategories().subscribe(data => {
      this.categories = data;
    });
  }
  onSelectCategory(event: any): void {
    const selectedCategory = event.target.value;

    // if they pick the select one option... show nothing
    if (selectedCategory == "") {
      this.matchingServices = [];
    }
    else {
      // otherwise... find the matching sayings and show
      this.servicesService.getServices().subscribe((data)=>{
        //console.log(data);
        // TODO
        this.matchingServices = data.filter(d => d.ServiceID.startsWith(selectedCategory.substring(0,4)) );  
     })
      // this.matchingServices =
      //   this.servicesService.getServices(
      //     selectedCategory);
    }
  }
}
