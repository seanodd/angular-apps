import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Saying } from './models/saying.model'
import { SayingsService } from './providers/sayings.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'sayings';
  // this is the data the HTML template binds to
  categories: Array<string> = [];
  // this is the matching sayings the HTML template binds to
  matchingSayings: Array<Saying> = [];
  personSearch: string = "";
  // receive the SayingsService we need to make calls to
  constructor(private sayingsService: SayingsService) {
  }
  // when the component loads, get the categories
  ngOnInit() {
    this.categories = this.sayingsService.getCategories();
  }
  // when they pick from the dropdown, get the matching sayings
  onSelectSaying(event: any): void {
    const selectedCategory = event.target.value;

    // if they pick the select one option... show nothing
    if (selectedCategory == "") {
      this.matchingSayings = [];
    }
    else {
      // otherwise... find the matching sayings and show
      this.matchingSayings =
        this.sayingsService.getSayingsThatMatchCategory(
          selectedCategory);
    }
  }

  onSearchClicked(): void {
    // const personSearch = event.target.value;
    //console.dir(event);
    // console.log(this.personSearch);
    // if they pick the select one option... show nothing
    if (this.personSearch == "") {
      this.matchingSayings = [];
    }
    else {
      // otherwise... find the matching sayings and show
      this.matchingSayings =
        this.sayingsService.getSayingsThatMatchPerson(
          this.personSearch);
    }
  }
}