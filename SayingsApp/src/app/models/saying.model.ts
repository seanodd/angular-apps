export class Saying {
    // properties
    public category: string = '';
    public quote: string = '';
    public person: string = '';
    constructor(category: string, quote: string, person: string) {
        this.category = category;
        this.quote = quote;
        this.person = person;
    }
}