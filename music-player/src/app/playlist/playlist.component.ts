import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SongsService } from '../providers/songs.service';
@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {

  songs: Array<any> = [];

  constructor(private router: Router,
    private songsService: SongsService) { }

  ngOnInit() {
    this.songs = this.songsService.getAllSongs();
  }

  playSong(id: string): void {
    this.router.navigate(['/player'],
      {
        queryParams: {
          id: id
        }
      });
  }
}
