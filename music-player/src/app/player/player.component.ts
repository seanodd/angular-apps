import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { SongsService } from '../providers/songs.service';


@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css'],
})
export class PlayerComponent {
  song!: any;
  url!: SafeResourceUrl;

  constructor(
    private activatedRoute: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private songsService :SongsService 
  ) { }
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      let id = params['id'];
      this.song = this.songsService.getSong(id);
      this.url =
        this.sanitizer.bypassSecurityTrustResourceUrl(this.song.src);
    });
  }
}