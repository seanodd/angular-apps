import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { Routes, RouterModule } from '@angular/router';


import { PlaylistComponent } from './playlist/playlist.component';
import { PlayerComponent } from './player/player.component';
import { SongsService } from './providers/songs.service';

const appRoutes: Routes = [
  { path: "", component: PlaylistComponent },
  { path: "playlist", component: PlaylistComponent },
  { path: "player", component: PlayerComponent },
  // { path: "contact-us", component: ContactUsComponent }
  ];

@NgModule({
  declarations: [
    AppComponent,
    PlaylistComponent,
    PlayerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [SongsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
