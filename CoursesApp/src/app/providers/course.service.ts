
import { Injectable } from '@angular/core';

import { Course } from '../models/course.model';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private http: HttpClient) { };

  // set up the endpoint and any HTTP headers
  private courseEndpoint: string =
    'http://localhost:8081/api/courses';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getCourses(): Observable<Course[]> {
    return this.http.get(this.courseEndpoint,
      this.httpOptions)
      .pipe(map(res => <Course[]>res));
  }
  getCourse(id: number): Observable<Course> {
    return this.http.get(this.courseEndpoint + "/" + id,
      this.httpOptions)
      .pipe(map(res => <Course>res));
  }
  // getCourse(id: number) : Observable<Course> {
  //   return this.http.get(this.courseEndpoint + "/" + id,
  //   this.httpOptions)
  //   .pipe(map(res => <Course>res));
    // }
}
