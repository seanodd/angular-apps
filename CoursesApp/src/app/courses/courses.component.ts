import { Component, OnInit } from '@angular/core';
import { Course } from '../models/course.model';
import { CourseService } from '../providers/course.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})

export class CoursesComponent implements OnInit {
  courses: Array<Course> = [];
  constructor(private courseService: CourseService) { }
  ngOnInit() {
    // call getCourses() method in CourseService
    this.courseService.getCourses().subscribe(data => {
      this.courses = data;
    });
  }
}
