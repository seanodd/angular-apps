export class Course {
        public id!: number;
        public dept: string = '';
        public courseNum!: number; 
        public courseName: string = '';
        public instructor: string = '';
        public startDate: string = '';
        public numDays!: number;
}
