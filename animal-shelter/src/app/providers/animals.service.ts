import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Animal } from './../models/animal.model';

@Injectable({
  providedIn: 'root'
})
export class AnimalsService {

  constructor(private http: HttpClient) { };

  // set up the endpoint and any HTTP headers
  private animalEndpoint: string =
    'https://localhost:7268/api/Animals';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getAnimals(): Observable<Animal[]> {
    return this.http.get(this.animalEndpoint,
      this.httpOptions)
      .pipe(map(res => <Animal[]>res));
  }
  getAnimal(id: number): Observable<Animal> {
    return this.http.get(this.animalEndpoint + "/" + id,
      this.httpOptions)
      .pipe(map(res => <Animal>res));
  }
  // register a animal and pass the server the values as:
  // AnimalName=some-animal&Coach=some-coach&CoachEmail=some-email
  postNewAnimal(name: string, species: string, breed: string,owner: string, checkInDate: string, checkOutDate: string): Observable<Animal> {
    return this.http.post<Animal>(this.animalEndpoint,
      {
        name: name,
        species: species,
        breed: breed,
        owner: owner,
        checkInDate: checkInDate,
        checkOutDate: checkOutDate         
      },
      this.httpOptions);
  }

  putAnimal(animalId: number, name: string, species: string, breed: string,owner: string, checkInDate: string, checkOutDate: string): Observable<Animal> {
    return this.http.put<Animal>(this.animalEndpoint + "/" + animalId,
      {
        animalId: animalId,
        name: name,
        species: species,
        breed: breed,
        owner: owner,
        checkInDate: checkInDate + "T00:00:00",
        checkOutDate: checkOutDate+ "T00:00:00"         
      },
      this.httpOptions);
  }

  deleteAnimal(animalId: number): Observable<number> {
    return this.http.delete<number>(this.animalEndpoint  + "/" + animalId);
  }
}
