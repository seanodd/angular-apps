import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Animal } from '../models/animal.model';
import { AnimalsService } from '../providers/animals.service';

@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.css']
})
export class AnimalComponent {

  animal!: Animal;

  animalId!: number;
  name: string = '';
  species: string = '';
  breed: string = '';
  owner: string = '';
  checkInDate: string = '';
  checkOutDate: string = '';
  title = 'Create a new Animal';

  constructor(
    private animalsService: AnimalsService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params['id']) {
        let id = params['id'];
        this.animalsService.getAnimal(id).subscribe((data) => {
          //console.log(data);
          this.animal = data;
          this.animalId = this.animal.animalId
          this.name = this.animal.name
          this.species = this.animal.species
          this.breed = this.animal.breed,
            this.owner = this.animal.owner,
            this.checkInDate = this.animal.checkInDate.split("T")[0],
            this.checkOutDate = this.animal.checkOutDate.split("T")[0],
            this.title = "Update a Animal"
        })
      }
    });
  }
  onSubmitAnimalClicked(): void {
    if (this.animalId) {
      this.animalsService.putAnimal(this.animalId, this.name, this.species, this.breed, this.owner, this.checkInDate, this.checkOutDate).subscribe((data: Animal) => {
        //console.log(data);
        this.animal = data;
      });
     }
    else {
      this.animalsService.postNewAnimal(this.name, this.species, this.breed, this.owner, this.checkInDate, this.checkOutDate).subscribe((data: Animal) => {
        //console.log(data);
        this.animal = data;
      });

    }
  }

}
