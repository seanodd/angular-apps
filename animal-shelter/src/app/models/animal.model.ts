export class Animal {
        public animalId!: number;
        public name: string = '';
        public species: string = '';
        public breed: string = '';
        public owner: string = '';
        public checkInDate: string = '';
        public checkOutDate: string = '';       
}
