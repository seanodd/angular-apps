import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Animal } from '../models/animal.model';
import { AnimalsService } from '../providers/animals.service';

@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.css']
})
export class AnimalsComponent {
  animals: Array<Animal> = [];
  animal!: Animal;
  deletedAnimalId!: number;
  constructor(
    private animalsService: AnimalsService,
    private router: Router
  ) { }
  ngOnInit() {

      this.animalsService.getAnimals().subscribe((data)=>{
        //console.log(data);
        // TODO
        this.animals = data;  
     });

  };
  onDeleteAnimalClicked(animalId: number): void {
  
    this.animalsService.deleteAnimal(animalId).subscribe((data: number) => {
      //console.log(data);
      this.deletedAnimalId = data;
      this.fetchData();
    });
    // this.router.navigate(['/animals']);
  };
  fetchData() {
    this.animalsService.getAnimals().subscribe((data)=>{
      //console.log(data);
      // TODO
      this.animals = data;  
   });
}
}
