import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnimalComponent } from './animal/animal.component';
import { AnimalsComponent } from './animals/animals.component';
import { HomeComponent } from './home/home.component';
const routes: Routes = [
  { path: "", component: HomeComponent }, 
  { path: "home", component: HomeComponent },
  { path: "animals", component: AnimalsComponent },
  { path: "animal", component: AnimalComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
