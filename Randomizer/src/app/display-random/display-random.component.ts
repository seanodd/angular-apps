import { Component } from '@angular/core';
import { RandomServiceService } from '../providers/random-service.service';

@Component({
  selector: 'app-display-random',
  templateUrl: './display-random.component.html',
  styleUrls: ['./display-random.component.css']
})
export class DisplayRandomComponent {
  randomNumber: number | undefined;

  constructor(private RandomServiceService: RandomServiceService) {
  }
  ngOnInit() {
    this.randomNumber = this.RandomServiceService.getRandomNumber();
  }
}
