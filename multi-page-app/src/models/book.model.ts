export class Book {
    // properties
    public title: string = '';
    public author: string = '';
    public isbn: string = '';
    constructor(title: string, author: string, isbn: string) {
        this.title = title;
        this.author = author;
        this.isbn = isbn;
    }
}
