import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  title = 'Home';
  message = 'under construction';
  intro = "Lorem ipsum dolor sit amet. Ea sapiente consequatur qui ullam perspiciatis est aspernatur ipsa qui soluta quas et error pariatur aut dicta veniam? Vel quod odio sit dolore voluptatem eos tempore consequatur. Qui nesciunt maiores sed eius laborum qui fuga omnis non earum autem non velit modi.";

  details = "Et consequuntur quae est enim corporis aut ullam nisi vel galisum tempore eum sequi iusto sed similique totam cum reprehenderit officiis. 33 voluptates molestiae sed quaerat earum est sunt eligendi qui quibusdam esse 33 Quis rerum.";
  
  conclusion = "Sed odio corporis aut galisum molestias eos veritatis consequatur qui sapiente error vel commodi voluptatum. Et sapiente velit sit explicabo voluptatem qui autem illo et nostrum rerum ut quaerat harum ex vero ipsa eum autem quaerat. Ea rerum nostrum sed voluptas voluptates rem eveniet dolore qui quaerat ullam non eius nihil eos expedita iure?";

}
