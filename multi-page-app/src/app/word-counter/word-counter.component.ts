import { Component } from '@angular/core';

@Component({
  selector: 'app-word-counter',
  templateUrl: './word-counter.component.html',
  styleUrls: ['./word-counter.component.css']
})
export class WordCounterComponent {
  title = 'Word Count';
  essay: string = "";
  wordCount: any;
  words: any;

  onFormSubmitted($event: Event) : void {
    // form submit logic here
    // console.log($event);
    // console.log(this.essay);
    //this.words = this.essay.length ? this.essay.length : 0;
    // this.words = this.wordCount ? this.wordCount.length : 0;  
    
    this.wordCount = this.essay ? this.essay.split(/\s+/) : 0;
    this.words = this.wordCount ? this.wordCount.length : 0;

    
    // $scope.words = 0;
    // $scope.wordCounter = function() {
    //     var wordCount = $scope.text ? $scope.text.split(/\s+/) : 0;
    //     $scope.words = wordCount ? wordCount.length : 0;
    // };


    }
}
