import { Component } from '@angular/core';
import { Contact } from '../models/contact.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-exercise',
  templateUrl: './form-exercise.component.html',
  styleUrls: ['./form-exercise.component.css']
})
export class FormExerciseComponent {
contact: Contact = { name: '', email: '', phoneNumber: "" };
// contact: Contact = null;
  onSubmit() {
    console.log(`Saving contact: ${JSON.stringify(this.contact)}`);
    }
}
