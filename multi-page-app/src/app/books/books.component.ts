import { Component, OnInit } from '@angular/core';
import { Book } from '../../models/book.model';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

    title = 'Books';
    categories : Array<string> = [];
    books : Array<Book> = [];
    constructor() {
      // this.categories =
      // ["Inspiration", "Money", "Staying Safe", "Statistics"];
      this.books = [
      new Book("Book1" ,"Author1", "sdfgsdfg"),
      new Book("Book2","Author2", "sdfgsdfg")
      ];
      }
      ngOnInit() { }
}
