import { Component } from '@angular/core';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent {
  title = 'Contact Us';
  message = 'under construction';
  name = "Detroit Tennis Company";
  email = "contact-us@detroittennisco.com";
  phone = "(313) 987-6543";

}
