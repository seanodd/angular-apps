import { Component } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent {
  title = 'Products';
  message = 'under construction';

  products = [
    { product1ImageURL: "assets/bab1.jpg", product1AltText: 'Item 1', imageName: "Racket 1" },
    { product1ImageURL: "assets/wilson_clash.jpg", product1AltText: 'Item 2', imageName: "Racket 2" },
    { product1ImageURL: "assets/head1.jpg", product1AltText: 'Item 3', imageName: "Racket 3" },
    { product1ImageURL: "assets/dunlop.jpg", product1AltText: 'Item 4', imageName: "Racket 4" }
  ];
}
